/*
* Autor: Fátima Azucena MC
* Fecha: 26_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
#include <string>
using namespace std;

class Inventario {
private:
    std::string referenciaP;
    int cantidad;
public:

    void setReferenciaP(const std::string& referenciaP){
        this->referenciaP = referenciaP;
    }
    void setCantidad (int cantidad){
        this->cantidad = cantidad;
    }

    const std::string& getReferenciaP () const {
        return referenciaP;
    }
    int getCantidad() const {
        return cantidad;
    }
};

int main() {

    std::string referenciaP;
    int cantidad;
    Inventario inventario;

    std::cout<<"\n\tIngrese la referencia del producto: ";
    std::cin>> referenciaP;
    std::cout<<"\tIngrese la cantidad del producto: ";
    std::cin>> cantidad;

    inventario.setReferenciaP(referenciaP);
    inventario.setCantidad(cantidad);

    std::cout<<"\n\tReferencia: " << inventario.getReferenciaP() << std::endl;
    std::cout<<"\tCantidad: " << inventario.getCantidad() << std::endl;
    return 0;

}


