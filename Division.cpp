/*
* Autor: Fátima Azucena MC
* Fecha: 21_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
using namespace std;
int cociente ( int numerador , int denominador );
int main () {

    // Declaración de variables
    int numerador;
    int denominador;
    int resultado;

    cout<<"\n\tIntroduzca un númerador entero: ";
    cin>>numerador;
    cout<<"\tIntroduzca un denominador entero: ";
    cin>>denominador;
    resultado = cociente(numerador,denominador);
    cout<<"\n\tNumerador: "<<numerador<<"\n\tDenominador: "<<denominador<<"\n\tResultado: "<<resultado<<"\n";

}
int cociente ( int numerador , int denominador ) {
    return ( numerador / denominador );
}
