#include <iostream>
#include <string>

// Función para encriptar un mensaje
void encriptar(std::string &mensaje, int clave) {
    for (size_t i = 0; i < mensaje.length(); i++) {
        char &caracter = mensaje[i];

        // Solo encriptamos letras mayúsculas y minúsculas
        if (caracter >= 'a' && caracter <= 'z') {
            caracter = (caracter - 'a' + clave) % 26 + 'a';
        } else if (caracter >= 'A' && caracter <= 'Z') {
            caracter = (caracter - 'A' + clave) % 26 + 'A';
        }
    }
}

// Función para desencriptar un mensaje
void desencriptar(std::string &mensaje, int clave) {
    encriptar(mensaje, 26 - clave); // Desencriptar es encriptar con la clave complementaria
}

int main() {
    std::string mensaje;
    int clave;

    std::cout << "Ingrese el mensaje a encriptar: ";
    std::getline(std::cin, mensaje);
    std::cout << "Ingrese la clave de encriptación: ";
    std::cin >> clave;
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    std::cout << "\nMensaje original: " << mensaje << std::endl;

    encriptar(mensaje, clave);
    std::cout << "Mensaje encriptado: " << mensaje << std::endl;

    desencriptar(mensaje, clave);
    std::cout << "Mensaje desencriptado: " << mensaje << std::endl;

    return 0;
}

