/*
* Autor: Fatima Azucena MC
* Fecha: 26_03_23
* Correo: fatimaazucenamartinez274@gmail.com
*/

#include <iostream>
#include <string>
using namespace std;

class Estudiante {
	private:
	    std::string nombres;
	    std::string apellidos;
	    std::string carnet;
	    std::string carrera;
	    int edad;
	public:

	    // Setters
	    void setNombres(const std::string& nombres){
	        this->nombres = nombres;
	    }
	    void setApellidos(const std::string& apellidos){
                this->apellidos = apellidos;
            }
            void setCarnet(const std::string& carnet){
                this->carnet = carnet;
            }
	    void setCarrera(const std::string& carrera){
                this->carrera = carrera;
            }
            void setEdad(int edad){
                this->edad = edad;
            }

	    // Getters
	    const std::string& getNombres () const {
	        return nombres;
	    }
	    const std::string& getApellidos () const {
                return apellidos;
            }
            const std::string& getCarnet () const {
                return carnet;
            }
            const std::string& getCarrera () const {
                return carrera;
            }
	    int getEdad() const {
	        return edad;
	    }
};

int main() {

    std::string nombres;
    std::string apellidos;
    std::string carnet;
    std::string carrera;
    int edad;
    Estudiante estudiante;

    std::cout<<"\n\tIngrese nombre(s): ";
    std::cin>> nombres;
    std::cout<<"\tIngrese apellidos: ";
    std::cin>> apellidos;
    std::cout<<"\tIngrese el carnet: ";
    std::cin>> carnet;
    std::cout<<"\tIngrese la carrera: ";
    std::cin>> carrera;
    std::cout<<"\tIngrese la edad: ";
    std::cin>> edad;

    estudiante.setNombres(nombres);
    estudiante.setApellidos(apellidos);
    estudiante.setCarnet(carnet);
    estudiante.setCarrera(carrera);
    estudiante.setEdad(edad);

    std::cout<<"\n\tNombres: " << estudiante.getNombres() << std::endl;
    std::cout<<"\tApellidos: " << estudiante.getApellidos() << std::endl;
    std::cout<<"\tCarnet: " << estudiante.getCarnet() << std::endl;
    std::cout<<"\tCarrera: " << estudiante.getCarrera() << std::endl;
    std::cout<<"\tEdad: " << estudiante.getEdad() << std::endl;

    return 0;

}
