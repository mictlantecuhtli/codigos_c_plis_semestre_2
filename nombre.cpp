#include <iostream>
#include <string>

class Persona {
private:
    std::string nombre;  // Variable privada para el nombre
    int edad;  // Variable privada para la edad

public:
    // Setter para el nombre
    void setNombre(const std::string& nombre) {
        this->nombre = nombre;
    }

    // Getter para el nombre
    const std::string& getNombre() const {
        return nombre;
    }

    // Setter para la edad
    void setEdad(int edad) {
        this->edad = edad;
    }

    // Getter para la edad
    int getEdad() const {
        return edad;
    }
};

int main() {
    Persona persona;

    // Usar los setters para asignar valores a las variables privadas
    persona.setNombre("Juan");
    persona.setEdad(25);

    // Usar los getters para obtener los valores de las variables privadas
    std::cout << "Nombre: " << persona.getNombre() << std::endl;
    std::cout << "Edad: " << persona.getEdad() << " años" << std::endl;

    return 0;
}
