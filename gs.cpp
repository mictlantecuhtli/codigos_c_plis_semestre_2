#include <iostream>
#include <string>

class Inventario {
private:
    std::string referenciaP; 
    int cantidad; 

public:
    // Función "get" para obtener la nombre del producto
    std::string getReferencia() {
        return referenciaP;
    }

    // Función "set" para establecer el nombre del producto
    void setReferencia(const std::string& nuevaReferencia) {
        referenciaP = nuevaReferencia;
    }

    // Función "get" para obtener la cantidad del producto
    int getCantidad() {
        return cantidad;
    }

    // Función "set" para establecer la cantidad del producto
    void setCantidad(int nuevaCantidad) {
        cantidad = nuevaCantidad;
    }
};

int main() {

    Inventario inventario;
    std::string referenciaP;
    int cantidad; 
    
    // Uso de las funciones "get" para obtener los valores y mostrarlos
    std::cout << "Referencia: " << inventario.getReferencia();
    std::getline(std::cin,referenciaP);
    std::cout << "Cantidad: " << inventario.getCantidad();
    std::cin >> cantidad;

    // Modificación de los valores utilizando las funciones "set"
    inventario.setReferencia(referenciaP);
    inventario.setCantidad(cantidad);
}
